package com.example.maprojectgoogle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.fragment.app.FragmentActivity;

import android.app.Activity;
import android.content.pm.PackageManager;
import android.location.LocationListener;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.util.Log;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.firebase.FirebaseError;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.List;

public class MapsActivity extends FragmentActivity implements OnMapReadyCallback {

    private GoogleMap mMap;
    private FirebaseDatabase mDataBase;
    private DatabaseReference databaseReference;
    ChildEventListener mChildEventListener;
    DatabaseReference mProfileRef = FirebaseDatabase.getInstance().getReference();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
        mDataBase = FirebaseDatabase.getInstance();
        databaseReference = mDataBase.getReference("Fond");
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        LatLng tagil = new LatLng(57.907605, 59.972211);
        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(tagil, 12f));
        mMap.addMarker(new MarkerOptions().position(tagil).title("Tagil marker")).setVisible(false);
        mMap.moveCamera(CameraUpdateFactory.newLatLng(tagil));

        googleMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
        databaseReference.addListenerForSingleValueEvent(new ValueEventListener() {

            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                for (DataSnapshot child : dataSnapshot.getChildren()){
                    String name = child.child("name").getValue().toString();
                    String adress = child.child("adress").getValue().toString();
                    String info = child.child("info").getValue().toString();
                    String contact = child.child("contact").getValue().toString();
                    String work = child.child("work").getValue().toString();
                    String lat = child.child("coor").getValue().toString();
                    String lng = child.child("coor2").getValue().toString();


                    double latitude = Double.parseDouble(lat);
                    double longitude = Double.parseDouble(lng);

                    LatLng loc = new LatLng(latitude, longitude);
                    Marker mark = googleMap.addMarker(new MarkerOptions().position(loc).title(name).snippet("\n"+info+"\n \n"+adress+"\n \n"+contact+"\n \n"+work));
                    mark.showInfoWindow();
                    mMap.setInfoWindowAdapter(new CustomInfoWindow(MapsActivity.this));

                }
            }
            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }
}

package com.example.maprojectgoogle;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.Marker;

public class CustomInfoWindow   implements GoogleMap.InfoWindowAdapter {


    View myView;

    public CustomInfoWindow(Context context) {
        myView=LayoutInflater.from(context).inflate(R.layout.markerinfo,null);
    }

    @Override
    public View getInfoWindow(Marker marker) {
        TextView textPickupTitle = ((TextView)myView.findViewById(R.id.txtPickupInfo));
        TextView textPickupSnippet = ((TextView)myView.findViewById(R.id.txtPickupSnippet));

        textPickupTitle.setText(marker.getTitle());
        textPickupSnippet.setText(marker.getSnippet());

        return myView;
    }

    @Override
    public View getInfoContents(Marker marker) {
        return null;
    }
}
